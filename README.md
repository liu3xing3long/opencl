# Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/opencl/badges/ubuntu16.04/build.svg)](https://gitlab.com/nvidia/opencl/commits/ubuntu16.04)

- [`runtime`, `runtime-ubuntu16.04`, (*runtime/Dockerfile*)](https://gitlab.com/opencl/cuda/blob/ubuntu16.04/runtime/Dockerfile)
- [`devel`, `latest`, `devel-ubuntu16.04` (*devel/Dockerfile*)](https://gitlab.com/nvidia/opencl/blob/ubuntu16.04/devel/Dockerfile)
